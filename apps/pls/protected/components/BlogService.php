<?php 

/**
 * Class that holds all HTTP Service calls for blog endpoints
 */
class BlogService
{

    /**
     * Retrieve feed from updates endpoint
     */
    public function getUpdates()
    {
        return $this->execute(Yii::app()->params['latestUpdatesFeedUrl']);
    }

    /**
     * Retrieve feed from all blogs endpoint
     */
    public function getBlogs()
    {
        return $this->execute(Yii::app()->params['latestBlogFeedUrl']);
    }

    /**
     * Execute HTTP request
     * @param string $source
     */
    private function execute($source)
    {
        Feed::$userAgent = Yii::app()->params['curlUserAgent'];
        Feed::$cacheDir = Yii::app()->params['latestUpdatesFeedCacheDir'];
        Feed::$cacheExpire = Yii::app()->params['latestUpdatesFeedCacheExp'];
        return Feed::loadRss($source); 
    }
}