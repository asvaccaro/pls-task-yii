<?php 
if(!empty($items))
{
    foreach($items as $item)
    {
        ?>
        <div class='col-md-12 update'>
            <h3><a href="<?= $item->link ?>" target="_blank"><?= $item->title?></a></h3>
            <p><?= $item->description ?></p>
        </div>
        <?php
    }
}