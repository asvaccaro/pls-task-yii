<?php
/**
 * @var HelpController     $this
 * @var SimpleXMLElement[] $updates
 */

$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('pls', 'Latest Updates');
$this->breadcrumbs = [
	Yii::t('pls', 'Latest Updates'),
];
?>
<h1><?= Yii::t('pls', 'Latest Updates') ?></h1>

<div class="row">
	<?php
	if (!empty($items)) 
	{
		$this->renderPartial('/partials/feeds/blogs', ['items' => $items]);
	}
	else 
	{
		?>
			<?= Yii::t('pls', 'No updates are available at this time.') ?>
		<?php
	}
	?>
</div>