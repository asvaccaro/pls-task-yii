<?php 

/**
 * Model for Blog post from supereval.com xml feed 
 */

class Blog
{
    /**
     * @var string $title
     */
    public $title;

    /**
     * @var string $description
     */
    public $description;

    /**
     * @var string $link
     */
    public $link;

    function __construct($item)
    {
        $this->title = $item->title;
        $this->link = $item->link;
        $this->description = $item->description;
        $this->formatDescription();

    }

    public function formatDescription()
    {
        
        $more = ' <a class="read-more" href="' . $this->link . '" target="_blank">Read more</a>';
        $this->description = trim(str_replace(' [&#8230;]', '...' . $more, $this->description));
        $this->description = preg_replace('/The post.*appeared first on .*\./', '', $this->description);
    }
}